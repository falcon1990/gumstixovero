=================================================================
README - DESCRIPTION RESEARCH
=================================================================
Author:		Boris Schneider
Date:		26.06.2018
Description:	Foud a solution to rightly start a QT5 
		application when the Gumstx with Yocot Rocko	
		is booting.
=================================================================
Date: 26.06.2018
- Actually the soft run at the boot but disapear after 2 seconds.
- The application is not really desapear because when the mouse
  moove the graphical part of the application apears back.
- It's like if the graphical application and the command line 
  are running at the same time....
- On my way to found a solution.

Date 27.06.2018
- The soft run now after the boot and doesn't disapear after a
  cuple of seconds.
- I found a script that is intended to run an application after
  the boot.
- The new problem is that it take a lot of time to start the
  soft after the boot.
- On my way to found a solution.

Date 19.07.2018
- Add the files that are needed to create the "service" with the
startup script to run an application on boot (BootService).
