========================================================================================
Gumstic Overo Project
========================================================================================
Author:		Schneider Boris
Date:		13.06.2018
Description:	Gather all the steps done in order to compile Qt5 code on ubuntu and 
		run it on the Gumstix overo.
========================================================================================
Folder description

Dev:	
Stuff that is actually in develoopment or in the process of resolution.

Qt5_Linux:
Steps to use the version of Qt5 that you need on linux Ubuntu 16.04 LTS (fr).

QtCreator_CrossCompilation:
The way to use QtCreator with Qt5 in linux Ubuntu 16.04 LTS to be able to cross
compile a code for the Gumstix Overo.

Yocto:
All the steps that are needed to have a SD card bootable with Yocto Rocko and Qt5 
libraries installed for the Gumstix Overo.
All the steps that are needed to have tools to be able to cross compile with 
QtCreator a Qt5 software from the linux Ubuntu 16.04 LTS to the Gumstix Overo.
========================================================================================