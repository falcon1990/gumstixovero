/*
 * Main.c
 *
 *  Author      : Boris Schneider
 *  Created on  : Juin 26, 2018
 *  Description : Example usage of the SSD1306 Driver API's.
 *                Adaptation QT5 library code from Vinay Divakar.
 */

/* Lib Includes */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
//#include <signal.h>

#include <QCoreApplication>
#include <QDebug>
#include <QTimer>

/* Header Files */
#include "I2C.h"
#include "SSD1306_OLED.h"
#include "example_app.h"
#include "alarmanim.h"

/* Externs - I2C.c */
extern I2C_DeviceT I2C_DEV_2;

/* Oh Compiler-Please leave me as is */
volatile unsigned char flag = 0;

/* Alarm Signal Handler */
void ALARMhandler(int sig)
{
    /* Set flag */
    flag = 5;
    printf("(Main)ALARM Handler: set flag done\r\n");
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    alarmanim alarmAnim;

    /* Initialize I2C bus and connect to the I2C Device */
    if(init_i2c_dev2(SSD1306_OLED_ADDR) == 0)
    {
        printf("(Main)i2c-2: Bus Connected to SSD1306\r\n");
    }
    else
    {
        printf("(Main)i2c-2: OOPS! Something Went Wrong\r\n");
        exit(1);
    }

    /* Register the Alarm Handler */
    alarmAnim.setHandle(ALARMhandler);
//  signal(SIGALRM, ALARMhandler);

    /* Run SDD1306 Initialization Sequence */
    display_Init_seq();

    while(1)
    {
        /* Clear display */
        clearDisplay();

        // draw a single pixel
        drawPixel(0, 1, WHITE);
        Display();
        usleep(1000000);
        clearDisplay();

        // draw many lines
        testdrawline();
        usleep(1000000);
        clearDisplay();

        // draw rectangles
        testdrawrect();
        usleep(1000000);
        clearDisplay();

        // draw multiple rectangles
        testfillrect();
        usleep(1000000);
        clearDisplay();

        // draw mulitple circles
        testdrawcircle();
        usleep(1000000);
        clearDisplay();

        // draw a white circle, 10 pixel radius
        fillCircle(SSD1306_LCDWIDTH/2, SSD1306_LCDHEIGHT/2, 10, WHITE);
        Display();
        usleep(1000000);
        clearDisplay();

        // draw a white circle, 10 pixel radius
        testdrawroundrect();
        usleep(1000000);
        clearDisplay();

        // Fill the round rectangle
        testfillroundrect();
        usleep(1000000);
        clearDisplay();

        // Draw triangles
        testdrawtriangle();
        usleep(1000000);
        clearDisplay();

        // Fill triangles
        testfilltriangle();
        usleep(1000000);
        clearDisplay();

        // draw the first ~12 characters in the font
        testdrawchar();
        usleep(5000000);
        clearDisplay();

        // Display "scroll" and scroll around
        testscrolltext();
        usleep(1000000);
        clearDisplay();

        // Display Texts and Numbers
        display_texts();
        Display();
        usleep(5000000);
        clearDisplay();

        // Display miniature bitmap
        display_bitmap();
        Display();
        usleep(1000000);

        // Display Inverted image and normalize it back
        display_invert_normal();
        clearDisplay();
        usleep(1000000);
        Display();

        // Generate Signal after 20 Seconds
    //    alarm(20);
        flag = 0;
        alarmAnim.alarmStart(10000);

        // draw a bitmap icon and 'animate' movement
        testdrawbitmap_eg();
        clearDisplay();
        usleep(1000000);
        Display();
        printf("Main: End of annimation\r\n");

        // Good bye fellas :)
        deeplyembedded_credits();
        Display();
        usleep(5000000);
    }

    alarmAnim.alarmEnd();
    return a.exec();
}
