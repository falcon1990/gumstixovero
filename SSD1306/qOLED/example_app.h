#ifndef _EXAMPLE_APP_H_
#define _EXAMPLE_APP_H_

/* Lib Includes */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include  <signal.h>

#include "SSD1306_OLED.h"

/* MACRO's */
#define LOGO16_GLCD_HEIGHT 16
#define LOGO16_GLCD_WIDTH  16
#define NUMFLAKES 10
#define XPOS 0
#define YPOS 1
#define DELTAY 2

/* Extern volatile */
extern volatile unsigned char flag;


#ifdef __cplusplus
extern "C"{
#endif
/* Function Declarations */
void testdrawline();
void testdrawrect();
void testfillroundrect();
void testfillrect();
void testdrawcircle();
void testdrawroundrect();
void testdrawtriangle();
void testfilltriangle();
void testdrawchar();
void testscrolltext();
void display_texts();
void display_texts();
void display_bitmap();
void display_invert_normal();
void testdrawbitmap(const unsigned char *bitmap, unsigned char w, unsigned char h);
void testdrawbitmap_eg();
void deeplyembedded_credits();
#ifdef __cplusplus
}
#endif

#endif // _EXAMPLE_APP_H_
