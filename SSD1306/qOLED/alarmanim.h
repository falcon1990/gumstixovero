#ifndef ALARMANIM_H
#define ALARMANIM_H

#include <QObject>
#include <QThread>

class alarmanim : public QThread
{
    Q_OBJECT

public:
    alarmanim();
    void setHandle(void(*fun)(int));
    void alarmStart(int t);
    void alarmStop();
    void alarmEnd();
    void run();

private:
    void(*cbFun)(int);
    bool enable;
    bool startTick;
    int speed;
};

#endif // ALARMANIM_H
