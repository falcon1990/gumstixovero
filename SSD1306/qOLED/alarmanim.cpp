/*
 * MIT License

Copyright (c) 2017 DeeplyEmbedded

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

 * alarmanim.cpp
 *
 *  Author      : Boris Schneider
 *  Created on  : Juin 26, 2018
 *  Description : Alarm "home made" because it's not posible to use
 *                the "alarm" and "signal" fonction from c library.
 *
 */

#include "alarmanim.h"
#include <QDebug>

alarmanim::alarmanim()
{
    this->enable = false;
    this->startTick = false;
    this->speed = 1000;
    this->cbFun = nullptr;

    //Start
    this->enable = true;
    this->start();
    qDebug() << "(alarmanim)constructor: timer created";
}

void alarmanim::setHandle(void(*fun)(int sig))
{
    this->cbFun = fun;
    qDebug() << "(alarmanim)setHandle: Handle connected";
}

void alarmanim::alarmStart(int t)
{
    this->speed = t;
    this->startTick = true;
    qDebug() << "(alarmanim)start: timer started";
}

void alarmanim::alarmStop()
{
    this->startTick = false;
    qDebug() << "(alarmanim)stop: timer stoped";
}

void alarmanim::alarmEnd()
{
    this->enable = false;
    qDebug() << "(alarmanim)end: timer ended";
}

void alarmanim::run()
{
    qDebug() << "(alarmanim)run: timer runing";
    while(this->enable == true)
    {
        while(this->startTick == true)
        {
            this->msleep(this->speed);
            if(this->cbFun != nullptr)
            {
                qDebug() << "(alarmanim)run: function called";
                this->cbFun(0);
            }
            else
            {
                qDebug() << "(alarmanim)run: function failed";
            }
            this->alarmStop();
        }
        this->msleep(100);
    }
}
